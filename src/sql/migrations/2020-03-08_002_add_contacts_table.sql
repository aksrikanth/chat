create table if not exists contacts (
  user_id integer,
  contact_id integer,
  blocked integer default 0, -- boolean
  created_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  updated_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  primary key (user_id, contact_id),
  foreign key (user_id) references users (user_id) on delete cascade,
  foreign key (contact_id) references users (user_id) on delete cascade
);
