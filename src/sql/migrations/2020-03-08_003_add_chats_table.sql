create table if not exists chats (
  chat_id integer primary_key,
  chat_type text not null,
  chat_name text,
  chat_hash text not null unique,
  owner_id integer,
  hidden integer default 0, -- boolean
  private integer default 0, -- boolean
  created_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  updated_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  foreign key (owner_id) references users (user_id) on delete cascade
);

create index idx_chats_hash on chats (chat_hash);
create index idx_chats_owner on chats (owner_id);
