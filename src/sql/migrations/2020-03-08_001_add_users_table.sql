create table if not exists users (
  user_id integer primary key,
  email text not null unique,
  display_name text,
  password_digest text not null,
  disabled integer default 0, -- boolean
  created_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  updated_at text -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
);

create unique index idx_users_email on users (email);
