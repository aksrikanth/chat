create table if not exists members (
  chat_id integer,
  member_id integer,
  read_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  muted integer default 0, -- boolean
  blocked integer default 0, -- boolean
  created_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  updated_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  primary key (chat_id, member_id),
  foreign key (chat_id) references chats (chat_id) on delete cascade,
  foreign key (member_id) references users (user_id) on delete cascade
);

create index idx_members_member on members (member_id);
