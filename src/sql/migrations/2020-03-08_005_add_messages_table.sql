create table if not exists messages (
  message_id integer primary key,
  sender_id integer,
  chat_id integer,
  message text not null,
  created_at text, -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
  updated_at text -- ISO8660 time string '2018-10-09T08:19:16.999578Z'
);

create index idx_messages_chat on messages (chat_id);
