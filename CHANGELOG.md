# Changelog

## Unreleased

### Added
- Readme
- License
- Changelog
- Dependency on Rocket for routing API calls