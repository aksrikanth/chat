# Chat

A chat service written in Rust.

## Introduction

Should support direct messages, group chat and rooms (like slack).

## Design

### DB Schema

A chat service handles messages. This means that messages are the fundamental component. A message needs at a minimum:

- Message text
- Sender (User)
- Conversation
- Timestamp

These messages then are organized into chats. A chat has two or more members. There are three kinds of chats we want to support:

1. 1:1 direct messages with only two members
1. 1:many group chat with more than two members and
1. chat rooms with arbitrary number of members

Chat rooms need an identifier that people can use to join and leave. They also need some way of restricting the members who can join (for private rooms).

A chat therefore needs:

- Type (DM, GROUP or ROOM)
- Name (unique among ROOM types, but may be used by GROUP types with no constraints)
- Private Flag (only used for ROOM type)

A chat also needs to track its members and where each member is in the chat (read status). Members need:

- Recipient (User)
- Last read message timestamp
- Mute Flag (Member wishes to ignore/block this chat)
- Notify Flag (Member wishes to be/not be notified of messages in this chat)

A chat service also needs to support relationships between users and maintain a list of known Contacts. A contact has:

- User
- Contact (User)
- Nickname
- Custom Avatar
- Block Flag

Finally a user has:

- Email (used to identify users)
- Display Name
- Avatar (Image)
- Password Digest

### DB Query Patterns

| Use Case | Param(s) | Notes |
|:---|:--:|:--|
| Find User by Email | `email` | |
| Fetch Contacts by Prefix | `prefix` | |
| Fetch Unread Messages from All Conversations | | query both `member` and `message` table using `chat_id` and filter messages newer than `member.last_read_timestamp` |
| Fetch New Messages from All Conversations | `last_fetched_timestamp` | query both `member` and `message` table using `chat_id` and filter messages newer than `last_fetched_timestamp` |
| Fetch Older Messages for a Conversation | `chat_id`, `before_timestamp` | |
| Send Message | `text`, `chat_id` | |
| Mark Conversation as Read | `chat_id` | updates the `member` table |

### API Design

### Data flow

## Implementation
